const fs = require('fs');
const path = require('path');
const { JSDOM } = require('jsdom');
const forKtxRend = require('./ktxRenderCustom.js');


// 特別なHTMLファイルのパスを指定
const headerFilePath = 'templates/header.html';

// 複数のHTMLファイルのディレクトリを指定
const htmlFilesDirectory = 'posts/';

const indexFilePath = 'index.html';

// ファイル名から日付とタイトルを抽出する関数
function extractInfoFromFileName(fileName) {
	const match = fileName.match(/^(\d{4}-\d{2}-\d{2})-(.*)\.html/);
	return match ? { date: match[1], title: match[2] } : null;
}

// 日付とタイトルを受け取り、ディレクトリパスを生成する関数
function generateDirectoryPath(date, title) {
	if (!date || !title) {
		console.error('Invalid date or title format');
		return null;
	}

	const [year, month, day] = date.split('-');
	const directoryPath = path.join('site', year, month, day, title);

	return directoryPath;
}

// 複数のHTMLファイルを非同期で読み込み
fs.readdir(htmlFilesDirectory, (err, files) => {
	if (err) {
		console.error('HTMLファイルの読み込みエラー:', err);
		return;
	}

	files.forEach((file) => {
		if (path.extname(file) === '.html') {
			const filePath = path.join(htmlFilesDirectory, file);

			// ファイル名から日付とタイトルを抽出
			const fileInfo = extractInfoFromFileName(file);

			// ディレクトリパスを生成
			const directoryPath = generateDirectoryPath(fileInfo.date, fileInfo.title);

			// ディレクトリが存在しない場合は作成
			if (directoryPath) {
				fs.mkdirSync(directoryPath, { recursive: true });
				console.log(`Directory created: ${directoryPath}`);
			}


			// 特別なHTMLファイルを非同期で読み込み
			fs.readFile(headerFilePath, 'utf8', (err, specialFileContent) => {
				if (err) {
					console.error('特別なHTMLファイルの読み込みエラー:', err);
					return;
				}


				// 読み込んだ特別なHTMLファイルに現在のHTMLファイルを挿入
				fs.readFile(filePath, 'utf8', (err, fileContent) => {
					if (err) {
						console.error('HTMLファイルの読み込みエラー:', err);
						return;
					}

					const generatedFilePath = path.join(directoryPath, 'index.html');

					//change title in header.html
					const domH = new JSDOM(specialFileContent);
					const domC = new JSDOM(fileContent);

					const documentH = domH.window.document;
					const documentC = domC.window.document;


					const titleOfPost = documentC.querySelector('h1').innerHTML
					documentH.title = `${titleOfPost} | blog.kitagy.net`;

					documentC.querySelector('time').innerHTML = fileInfo.date;


					const modifiedHeader = domH.serialize();
					const modifieContent = forKtxRend.ktxRenderCustom(domC);

					// insert header
					const updatedFileContent = modifiedHeader.replace('<!-- {% include html files %} -->', modifieContent);

					// generate new html files to site/ directory
					fs.writeFile(generatedFilePath, updatedFileContent, (err) => {
						if (err) {
							console.error(`エラー${generatedFilePath}:`, err);
						} else {
							console.log(`${generatedFilePath}を作成`);
						}
					});
				});
			});
		}
	});

	// HTMLファイルのみをフィルタリング
	const htmlFiles = files.filter(file => path.extname(file) === '.html');

	// ファイルを日付順にソート
	htmlFiles.sort((a, b) => {
		const dateA = new Date(a.split('-')[0]);
		const dateB = new Date(b.split('-')[0]);
		return dateB - dateA;
	});


	// HTMLファイルの一覧を表示するHTMLを構築
	const htmlList = htmlFiles.map(file => {
		const filePath = path.join(htmlFilesDirectory, file);

		// ファイルを読み込んでh1タグからタイトルを取得
		const fileContent = fs.readFileSync(filePath, 'utf-8');
		const titleMatch = fileContent.match(/<h1>(.*?)<\/h1>/i);
		const title = titleMatch ? titleMatch[1] : 'No Title';

		// summaryクラスから記事の要約を取得
		const summaryMatch = fileContent.match(/<div id="summary">(.*?)<\/div>/i);
		const summary = summaryMatch ? summaryMatch[1] : 'No Summary';

		// 正規表現を使用して日付を取得
		const dateMatch = file.match(/^(\d{4}-\d{2}-\d{2})-(.*).html$/);
		const date = dateMatch[1].replace(/-/g, '/');

		// リンクを指定された形式に修正
		const link = file.replace('.html', '').replace(/-/g, '/');

		return `
      <li>
        <a href="${link}/">${title}</a> - ${date}
        <p>${summary}</p>
      </li>`;
	}).join('\n');

	// index.html ファイルを読み込む
	fs.readFile(indexFilePath, 'utf-8', (err, indexContent) => {
		if (err) {
			return console.log('index.html ファイルの読み込みエラー:', err);
		}

		// <!-- {% include post list %} --> というコメントの前に一覧を挿入
		const updatedIndexContent = indexContent.replace('<!-- {% include post list %} -->', `
      <ul class = "post-list">
        ${htmlList}
      </ul>
    `);

		// 更新された index.html ファイルを書き込む
		fs.writeFile('site/index.html', updatedIndexContent, (err) => {
			if (err) {
				console.log('index.html ファイルの書き込みエラー:', err);
			} else {
				console.log('index.html ファイルが正常に更新されました。');
			}
		});
	});

	fs.copyFileSync("css/style.css", "site/css/style.css");

});
