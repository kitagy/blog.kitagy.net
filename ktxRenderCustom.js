var http = require("http");
var fs = require('fs');
const { JSDOM } = require('jsdom');
const katex = require('katex');

exports.ktxRenderCustom = function(dom) {

  // const dom = new JSDOM(data);
  const document = dom.window.document;

  // Render LaTeX math expressions
  document.querySelectorAll('.katex-math-display').forEach((element) => {
    const mathContent = element.textContent;
    const mathRendered = katex.renderToString(mathContent, {
      throwOnError: false, // Set to true to throw errors for invalid LaTeX
      displayMode: true,
    });
    element.innerHTML = mathRendered;
  });
  document.querySelectorAll('.katex-math').forEach((element) => {
    const mathContent = element.textContent;
    const mathRendered = katex.renderToString(mathContent, {
      throwOnError: false, // Set to true to throw errors for invalid LaTeX
    });
    element.innerHTML = mathRendered;
  });

  // Serve the modified HTML
  const modifiedHTML = dom.serialize();
  //  console.log(modifiedHTML);
  return modifiedHTML; 
}